package com.example.taskbook;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {


    ArrayList<Task> states = new ArrayList<Task>();
    TaskStorage taskStorage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        TaskStorage.Init(this);
        taskStorage = TaskStorage.getInstance();
        refreshTasks();

        FloatingActionButton fab = findViewById(R.id.buttonAdd);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTaskActivity();
            }
        });

    }

    private ArrayList<Task> getTasksList ()
    {
        try {
            return taskStorage.getTasks();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new ArrayList<Task>();
    }

    private void addTaskActivity()
    {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra("action", "add");
        startActivity(intent);
    }

    private void setInitialData(){
        Calendar c = Calendar.getInstance();

        states.add(new Task ("Бразилия", "Бразилиа", c, Task.Priority.high));
        states.add(new Task ("Аргентина", "Буэнос-Айрес", c, Task.Priority.high));
        states.add(new Task ("Колумбия", "Богота", c, Task.Priority.high));
        states.add(new Task ("Уругвай", "Монтевидео", c, Task.Priority.high));
        states.add(new Task ("Чили", "Сантьяго", c, Task.Priority.high));
        states.add(new Task ("Бразилия", "Бразилиа", c, Task.Priority.high));
        states.add(new Task ("Аргентина", "Буэнос-Айрес", c, Task.Priority.high));
        states.add(new Task ("Колумбия", "Богота", c, Task.Priority.high));
        states.add(new Task ("Уругвай", "Монтевидео", c, Task.Priority.high));
        states.add(new Task ("Чили", "Сантьяго", c, Task.Priority.high));
        states.add(new Task ("Бразилия", "Бразилиа", c, Task.Priority.high));
        states.add(new Task ("Аргентина", "Буэнос-Айрес", c, Task.Priority.high));
        states.add(new Task ("Колумбия", "Богота", c, Task.Priority.high));
        states.add(new Task ("Уругвай", "Монтевидео", c, Task.Priority.high));
        states.add(new Task ("Чили", "Сантьяго", c, Task.Priority.high));
        states.add(new Task ("Бразилия", "Бразилиа", c, Task.Priority.high));
        states.add(new Task ("Аргентина", "Буэнос-Айрес", c, Task.Priority.high));
        states.add(new Task ("Колумбия", "Богота", c, Task.Priority.high));
        states.add(new Task ("Уругвай", "Монтевидео", c, Task.Priority.high));
        states.add(new Task ("Чили", "Сантьяго", c, Task.Priority.high));
        states.add(new Task ("Бразилия", "Бразилиа", c, Task.Priority.high));
        states.add(new Task ("Аргентина", "Буэнос-Айрес", c, Task.Priority.high));
        states.add(new Task ("Колумбия", "Богота", c, Task.Priority.high));
        states.add(new Task ("Уругвай", "Монтевидео", c, Task.Priority.high));
        states.add(new Task ("Чили", "Сантьяго", c, Task.Priority.high));
        states.add(new Task ("Бразилия", "Бразилиа", c, Task.Priority.high));
        states.add(new Task ("Аргентина", "Буэнос-Айрес", c, Task.Priority.high));
        states.add(new Task ("Колумбия", "Богота", c, Task.Priority.high));
        states.add(new Task ("Уругвай", "Монтевидео", c, Task.Priority.high));
        states.add(new Task ("Чили", "Сантьяго", c, Task.Priority.high));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear:
                clearTask();
                return true;

            case R.id.action_exit:
                exit();
                return true;
            default:
                return true;
        }
    }

    private void clearTask()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Отчистить список задач")
                .setMessage("Вы действительно хотите отчистить список задач?")
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        taskStorage.deleteTasks();
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setCancelable(true);
        builder.show();
        refreshTasks();
    }

    private void exit ()
    {
        this.finishAffinity();
    }

    public void refreshTasks()
    {
        states = getTasksList();
        TextView noTask = findViewById(R.id.textViewNoTask);
        if (states.size() != 0)
            noTask.setVisibility(View.INVISIBLE);
        else
            noTask.setVisibility(View.VISIBLE);

        RecyclerView recyclerView = findViewById(R.id.list);
        TaskAdapter adapter = new TaskAdapter(this, states);
        recyclerView.setAdapter(adapter);
    }

    public void editTask (Task task)
    {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra("action", "edit");
        intent.putExtra("id", task.getId());
        intent.putExtra("name", task.getName());
        intent.putExtra("description", task.getDescription());
        String date = (new SimpleDateFormat(TaskStorage.pattern, TaskStorage.locale))
                .format(task.getDateAndTime().getTime());
        intent.putExtra("dateAndTime", date);
        intent.putExtra("priority", Task.stringPriority(task.getPriority()));
        startActivity(intent);
    }
}