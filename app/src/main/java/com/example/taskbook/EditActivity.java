package com.example.taskbook;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class EditActivity extends AppCompatActivity {
    TaskStorage taskStorage;

    boolean modeAdd = true;

    Calendar dateAndTime = GregorianCalendar.getInstance();
    TextView currentDate, currentTime;
    Task task = new Task();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        String action = getIntent().getStringExtra("action");
        if (action.equals("add")) initAdd();
        else if (action.equals("edit")) initEdit();

        taskStorage = TaskStorage.getInstance();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        currentDate = findViewById(R.id.textViewData);
        currentTime = findViewById(R.id.textViewTime);

        setInitialDate();
        setInitialTime();
    }

    private void initAdd()
    {
        modeAdd = true;
        Button buttonSubmit = findViewById(R.id.buttonSubmit);
        buttonSubmit.setText("Добавить");
    }

    private void initEdit()
    {
        modeAdd = false;

        int id = getIntent().getIntExtra("id", -1);
        String name = getIntent().getStringExtra("name");
        String description = getIntent().getStringExtra("description");

        Calendar dateAndTime = GregorianCalendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(TaskStorage.pattern, TaskStorage.locale);
        String date = getIntent().getStringExtra("dateAndTime");
        try {
            dateAndTime.setTime(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String priorityStr = getIntent().getStringExtra("priority");
        Task.Priority priority = Task.parsePriority(priorityStr);

        task.setId(id);
        task.setName(name);
        task.setName(description);
        task.setDateAndTime(dateAndTime);
        task.setPriority(priority);

        EditText editTextName = (EditText)findViewById(R.id.editTextName);
        editTextName.setText(name);
        EditText editTextDescription = (EditText)findViewById(R.id.editTextDescription);
        editTextDescription.setText(description);
        this.dateAndTime = dateAndTime;
        Spinner spinnerPriority = (Spinner)findViewById(R.id.spinnerPriority);

        ArrayAdapter<String> listSelection = (ArrayAdapter<String>)spinnerPriority.getAdapter();
        spinnerPriority.setSelection(listSelection.getPosition(priorityStr));

        Button buttonSubmit = findViewById(R.id.buttonSubmit);
        buttonSubmit.setText("Изменить");
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        return true;
    }

    public void setDateButton(View v)
    {
        new DatePickerDialog(EditActivity.this, d,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    public void setTimeButton(View v) {
        new TimePickerDialog(EditActivity.this, t,
                dateAndTime.get(Calendar.HOUR_OF_DAY),
                dateAndTime.get(Calendar.MINUTE), true)
                .show();
    }

    DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, monthOfYear);
            dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialDate();
        }
    };

    private void setInitialDate() {
        currentDate.setText("Дата: " + DateUtils.formatDateTime(this,
                dateAndTime.getTimeInMillis(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR));
    }

    private void setInitialTime() {
        currentTime.setText("Время: " + DateUtils.formatDateTime(this,
                dateAndTime.getTimeInMillis(),
                DateUtils.FORMAT_SHOW_TIME));
    }

    TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            dateAndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            dateAndTime.set(Calendar.MINUTE, minute);
            setInitialTime();
        }
    };

    public void submit(View v)
    {
        String name = String.valueOf(((EditText)findViewById(R.id.editTextName)).getText());
        String description = String.valueOf(((EditText)findViewById(R.id.editTextDescription)).getText());
        String priority = (String) ((Spinner)findViewById(R.id.spinnerPriority)).getSelectedItem();
        if (name.length() == 0)
        {
            Toast.makeText(this, "Введите название задачи", Toast.LENGTH_LONG).show();
            return;
        }

        task.setName(name);
        task.setDescription(description);
        task.setDateAndTime(dateAndTime);
        task.setPriority(Task.parsePriority(priority));

        if (modeAdd)
            taskStorage.addTask(task);
        else
            taskStorage.editTask(task);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}