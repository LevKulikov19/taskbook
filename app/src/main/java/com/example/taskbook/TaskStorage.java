package com.example.taskbook;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class TaskStorage {
    private static TaskStorage instance;

    private String dbName = "tasks";
    private String tableName = "tasks";
    private SQLiteDatabase db;
    private int endID = -1;

    public static Locale locale = Locale.getDefault();
    public static String pattern = "yyyy MMM dd HH:mm:ss";


    /*
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
    cal.setTime(sdf.parse("Mon Mar 14 16:02:37 GMT 2011"))
     */

    private TaskStorage(MainActivity activity)
    {
        db = activity.getBaseContext().openOrCreateDatabase(dbName + ".db", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS " + dbName + " (id INTEGER NOT NULL PRIMARY KEY, name TEXT, description TEXT, dateAndTime TEXT, priority TEXT)");
    }

    public static void Init (MainActivity activity) {
        instance = new TaskStorage(activity);
    }

    public static TaskStorage getInstance( ) {
        return instance;
    }

    public ArrayList<Task> getTasks() throws ParseException {
        ArrayList<Task> tasks = new ArrayList<Task>();
        Cursor query = db.rawQuery("SELECT * FROM tasks;", null);

        while(query.moveToNext()){
            String name = query.getString(1);
            String description = query.getString(2);

            Calendar dateAndTime = GregorianCalendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat(pattern, locale);
            String date = query.getString(3);
            dateAndTime.setTime(sdf.parse(date));


            Task.Priority priority = Task.Priority.valueOf(query.getString(4));

            Task task = new Task(name, description, dateAndTime, priority);
            task.setId(Integer.parseInt(query.getString(0)));
            tasks.add(task);

        }
        if (tasks.size() == 0) endID = 0;
        else endID = tasks.get(tasks.size()-1).getId();
        return sort(tasks);
    }

    private ArrayList<Task> sort (ArrayList<Task> tasks)
    {
        ArrayList<Task> result = new ArrayList<Task>();
        ArrayList<Task> low = new ArrayList<Task>();
        ArrayList<Task> medium = new ArrayList<Task>();
        ArrayList<Task> high = new ArrayList<Task>();

        for (Task task: tasks) {
            if (task.getPriority() == Task.Priority.low)
                low.add(task);
            else if (task.getPriority() == Task.Priority.medium)
                medium.add(task);
            else if (task.getPriority() == Task.Priority.high)
                high.add(task);
        }

        result.addAll(high);
        result.addAll(medium);
        result.addAll(low);
        return result;
    }

    public void addTask(Task task) {
        if (task.getId() == -1) task.setId(endID+1);
        endID += 1;
        String date = (new SimpleDateFormat(pattern, locale)).format(task.getDateAndTime().getTime());
        String q = "INSERT OR IGNORE INTO " + dbName + " VALUES ('" +
                task.getId() + "', '" +
                task.getName() + "', '" +
                task.getDescription() +"', '" +
                date + "' ,'" +
                task.getPriority().toString() + "');";
        db.execSQL(q);
    }

    public void editTask(Task task) {
        String date = (new SimpleDateFormat(pattern, locale)).format(task.getDateAndTime().getTime());
        String q = "UPDATE " + dbName + " SET name = '" +
                task.getName() + "', description = '" +
                task.getDescription() +"', dateAndTime = '" +
                date + "' , priority = '" +
                task.getPriority().toString() + "' WHERE id = " + task.getId();
        db.execSQL(q);
    }

    public void deleteTasks() {
        db.execSQL("delete from "+ dbName);
    }

    public void deleteTask(Task task) {
        db.execSQL("delete from "+ dbName + " WHERE id = " + task.getId());
    }

    public void deleteTaskById (int id) {
        db.execSQL("delete from "+ dbName + " WHERE id = " + id);
    }
}
