package com.example.taskbook;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {

    private final LayoutInflater inflater;
    private final List<Task> states;
    private MainActivity context;

    TaskAdapter(MainActivity context, List<Task> states) {
        this.states = states;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }
    @Override
    public TaskAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TaskAdapter.ViewHolder holder, int position) {
        Task task = states.get(position);
        holder.nameView.setText(task.getName());

        //holder.dateView.setText(task.getDateAndTime().getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));
        //holder.timeView.setText(task.getDateAndTime().getDisplayName(Calendar.AM_PM, Calendar.LONG, Locale.getDefault()));

        String date = DateUtils.formatDateTime(context,
                task.getDateAndTime().getTimeInMillis(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR);
        holder.dateView.setText(date);

        String time = DateUtils.formatDateTime(context,
                task.getDateAndTime().getTimeInMillis(),
                DateUtils.FORMAT_SHOW_TIME);
        holder.timeView.setText(time);

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TaskStorage.getInstance().deleteTask(task);
                context.refreshTasks();
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.editTask(task);
            }
        });
    }

    @Override
    public int getItemCount() {
        return states.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView nameView, dateView, timeView;
        final ImageView delete, edit;
        ViewHolder(View view){
            super(view);
            nameView = view.findViewById(R.id.name);
            dateView = view.findViewById(R.id.date);
            timeView = view.findViewById(R.id.time);
            delete = view.findViewById(R.id.imageViewDelete);
            edit = view.findViewById(R.id.imageViewEdit);
        }
    }
}
