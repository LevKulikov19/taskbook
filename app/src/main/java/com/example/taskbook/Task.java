package com.example.taskbook;

import java.util.Calendar;

public class Task {
    public enum Priority{
        low,
        medium,
        high
    }

    private int id = -1;
    private String name = "";
    private String description = "";
    private Calendar dateAndTime = Calendar.getInstance();
    private Priority priority = Priority.low;

    public Task(String name, String description, Calendar dateAndTime, Priority priority){

        this.name=name;
        this.description = description;
        this.dateAndTime = dateAndTime;
        this.priority = priority;
    }

    public Task(){

    }

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getDateAndTime() {
        return this.dateAndTime;
    }

    public void setDateAndTime(Calendar dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public Priority getPriority() {
        return this.priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public static Priority parsePriority(String priority)
    {
        if (priority.equals("Низкий")) return Priority.low;
        else if (priority.equals("Средний")) return Priority.medium;
        else if (priority.equals("Высокий")) return Priority.high;
        else return Priority.low;
    }

    public static String stringPriority(Priority priority)
    {
        if (priority == Priority.low) return "Низкий";
        else if (priority == Priority.medium) return "Средний";
        else if (priority == Priority.high) return "Высокий";
        else return "Низкий";
    }

}
